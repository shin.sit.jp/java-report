import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel topLabel;
    private JButton potatoButton;
    private JButton yakisobaButton;
    private JButton karaageButton;
    private JButton udonButton;
    private JButton ramenButton;
    private JButton tempuraButton;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JLabel Kakaku;
    int totalprice=0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().topLabel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            String t=textPane1.getText();
            textPane1.setText(t+food+"\n");
            int price=100;
            totalprice+=price;
            Kakaku.setText(totalprice+" yen");
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible.");
        }
    }

    public FoodOrderingMachine() {
        potatoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Potato");
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int check=JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "checkout",
                        JOptionPane.YES_NO_OPTION);
                if(check==0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you.The total price is "+totalprice+" yen.");
                    textPane1.setText("");
                    totalprice=0;
                    Kakaku.setText(totalprice+" yen");
                }
            }
        });
    }
}


